---
title: 工具
layout: page
---
* markdown在线编辑器<a href="http://markdown-editor.wicp.net" target=_blank>markdown-editor</a>

* 查小米<a href="http://chayixia.wicp.net/xiaomi" target=_blank>xiaomi</a>【由于隐私原因，网站已暂停查询】

* 查开房<a href="http://chayixia.wicp.net/chacha" target=_blank>chayixia</a>【由于隐私原因，网站已暂停查询】

* 分享巴巴<a href="http://tech2film.wicp.net" target=_blank>论坛</a>

* yacy搜索<a href="http://chayixia.wicp.net:8090" target=_blank>yacy</a>

* web开发在线教程<a href="http://web3w.wicp.net" target=_blank>web3w</a>

* 欢迎来吐槽<a href="http://web3w.wicp.net/chat.html" target=_blank>chat online</a>

* 聊天室(需要翻墙)<a href="http://richardxxx0x1.appspot.com/" target=_blank>自己架的聊天室</a>

* linux command usage<a href="http://web3w.wicp.net/linux_command.pdf" target=_blank>linux command</a>
