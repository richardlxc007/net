---
layout: post
date: Fri Apr 18 08:27:47 EDT 2014
title: 高效率写博客
categories: 分享
tags: vim
---
最近写博客，发现效率很低，今晚就搞个模板来提高效率：

首先配置`~/.vimrc`文件:

    map ,dt a<C-R>=strftime('%Y-%m-%d %H:%M:%S')<CR>

然后搞个模板，如`post_template.txt`

    ---
    layout: post
    date:
    title:
    categories:
    tags:
    ---

当写博客时候，就可以在vim的底行模式下执行`:r post_template.txt` 导入模板，然后在命令行模式执行`,dt`就可以插入写文章的时间了:)

** 其他方法: **

1. 也可以通过`:r !date` or `!!date`插入时间；
2. 其实也可以通过在`~/.vim/template`文件夹中建立模板文件，比如上面的`post_template.txt`；然后在`~/.vimrc`中添加代码：

```
autocmd BufNewFile *.md 0r ~/.vim/template/post_template.txt
```

这时如果再编辑.md文件时候，就会自动添加模板了.

---
免费linux开发环境 ，直接点击链接就可以了 <a href="https://www.nitrous.io/" target=_blank>nitrous</a>
---
                                                                                                                                                               
---                                                                                                                                                            
免费vpn账号链接：<a href="http://www.djjsq.com" target=_blank>豆荚加速</a>                 
---  
