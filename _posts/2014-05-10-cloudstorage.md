---
layout: post
date: Sat May 10 22:16:49 HKT 2014
title: 通过命令行上传文件到dropbox or onedrive
categories: 分享
tags: linux
---
相信大家已经接触到很多网盘了，比如国内常见的几家：

- <a href="http://pan.baidu.com" target=_blank>百度网盘</a>
- <a href="http://115.com" target=_blank>115网盘</a>
- <a href="http://yunpan.360.cn" target=_blank>360云盘</a>
- <a href="http://www.dbank.com" target=_blank>华为网盘</a>
- <a href="https://vdisk.weibo.com" target=_blank>新浪微盘</a>

还有其他几家网盘，就不列了。

上面几个我最常用的就是百度网盘以及115网盘还有360，原因有一下几点：

- 支持离线下载功能(全部都是免费的，用的最多的功能了)
- 网盘容量都很大
- 上传下载速度都不错(下载、上传用的很少，几乎不用)
- 等等.

在国内网盘存的都是视频、pdf等资源。

除了国内的几家，国外也有很多非常有名的网盘：

- 谷歌网盘Gdrive https://drive.google.com  需要翻墙才能访问，如何翻?可以看下我之前写的博客 <a href="https://richardxxx0x.wordpress.com/2014/03/21/%E7%BF%BB%E5%A2%99%E4%BB%A3%E7%90%86%E8%BD%AF%E4%BB%B6/" target=_blank>翻墙代理软件</a>
- <a href="https://dropbox.com" target=_blank>Dropbox</a>
- <a href="https://onedrive.live.com/" target=_blank>Onedrive</a> (以前叫skydrive)
- <a href="https://www.box.com/" target=_blank>box</a>

国外的网盘和国内网盘相比，有很多优点，就举一个例子吧，在线编辑功能。上述列出的三个国外的产品都支持在线编辑功能，国内的就不用说了。缺点也有：容量；这里边也有篇文章介绍了<a href="http://www.geekfan.net/1649/" target=_blank>SkyDrive vs Google Drive – 谁是办公效率上的王者</a>

自己的私人资源一般都放到谷歌网盘以及dropbox.如果你还不知道选用哪个网盘，可以参考下这篇文章： <a href="http://www.ithome.com/html/soft/73786.htm" target=_blank>网盘哪个好：OneDrive/Dropbox/Box</a>

下面说说文章的重点：“通过命令行上传文件到dropbox 或者 onedrive”

如果有人用“无桌面”操作系统，如vps、自己的远程主机等，偶尔需要备份资料到网盘，那么通过命令行是再合适不过的了，可能也有人会选择先将资料通过scp或者ftp等下载到本地，再同步到网盘。这也是可以的，但是就效率而言，是低下的。下面就介绍下命令行上传或者下载资源到网盘：

- ### 方式1.通过命令行对dropbox网盘进行操作：
 
这个工具就是个bash脚本，下载地址<a href="https://www.dropbox.com/s/h5dwdjixiqdsdbx/dropbox_uploader.sh" target=_blank>dropbox_uploader</a>

使用也很简单，下载脚本，执行 bash dropbox_uploader.sh 

根据提示进行操作即可。

这个命令行工具支持对网盘文件的以下一些操作：

<script src="https://gist.github.com/hellokitty111/c2e29b3e73d08642b088.js"></script>

下面截图就是我通过命令行工具，上传、分享这个工具的过程：

![](http://pictureimage.qiniudn.com/Screenshot%202014-05-10%20at%2022.58.16.png)

- ### 方式2.【官方】除了上面这种方式，还可以通过安装官方的linux客户端来使用命令行，可以参考：

<a href="https://www.dropbox.com/install?os=lnx" target=_blank>通过命令行安装 Dropbox</a>

这种方式就是把本地文件和云端文件进行同步，如果你硬盘容量较大或者需要经常访问这些文件，推荐这种方式。相反，如果你只是想备份、想分享，不是经常访问这些文件，可以参考第一种方式。就例如我自己的chromebook，本地固态硬盘容量只有16G，目前也只剩下6.1G的容量，所以我是采用了第一种方式，即：有些文件上传到网盘，备份或者分享之后，就将本地文件删除。

除了上述的同步到服务器，dropbox也支持局域网同步任意多文件，可以参考这篇文章：

<a href="http://piaoyun.cc/Dropbox-mklink.html" target=_blank>Dropbox支持局域网及同步任意多个文件夹的解决方法</a>

- ### 通过命令行对微软onedrive网盘进行操作：

可以看下github上的开源项目：

<a href="https://github.com/mk-fg/python-onedrive" target=_blank>python-onedrive</a>

怎么用，README.md文件中已经说的很详细了。

在安装过程中，可能会遇到一些小问题，比如需要requests包之类的。根据错误提示，也很容易解决。

都可以通过pip来安装缺少的包，如： pip install MissingPackage

其帮助选项如下：

<script src="https://gist.github.com/hellokitty111/3f7e1d77456f65e430f2.js"></script>

最常用的也就是上传(put)、下载(get)、分享(link)、删除(rm)、移动(mv)等操作选项.

---
免费linux开发环境 ，直接点击链接就可以了 <a href="https://www.nitrous.io/" target=_blank>nitrous</a>
---

---
免费vpn账号链接：<a href="http://www.djjsq.com" target=_blank>豆荚加速</a>
---