---
layout: post
date: Sat Apr 19 09:26:51 CST 2014
title: 修改美国vps主机时间为北京时间
categories: 分享 
tags: linux
---
如果购买美国vps的话，主机时间就是美国时间，与北京时间相差十多个小时，看起来很不爽。在没修改时差之前，美国时间如下所示：

    [root@140322in04 kali]# date
    Fri Apr 18 21:13:43 EDT 2014

可以通过下面命令完成时区的修改：

    [root@140322in04 kali]# rm -rf /etc/localtime
    [root@140322in04 kali]# ln -s /usr/share/zoneinfo/Asia/Shanghai /etc/localtime

再执行命令查看：

    [root@140322in04 kali]# date
    Sat Apr 19 09:43:10 CST 2014

可以看到日期由美国的18号变为中国时间19号;

** 更简单的方法： **

直接修改文件 `~/.bashrc` 添加代码：

    export TZ="/usr/share/zoneinfo/Asia/Shanghai"
  
然后执行：
 
    [kali@140322in04 kali] source ~/.bashrc

这样就可以修改时区了.

上面这种简单方法适合没有root权限的用户，比如我在<a href="https://www.nitrous.io/?utm_source=nitrous.io&utm_medium=copypaste&utm_campaign=referral" target=_blank>nitrous</a>上没有root权限，只能通过上面这种简单方法来修改了。

---
免费linux开发环境 ，直接点击链接就可以了 <a href="https://www.nitrous.io/" target=_blank>nitrous</a>
---
                                                                                                                                                               
---                                                                                                                                                            
免费vpn账号链接：<a href="http://www.djjsq.com" target=_blank>豆荚加速</a>                 
---  
