---
layout: post
date: Fri May  2 22:12:54 HKT 2014
title: ssh2nitrous失败
categories: 分享
tags: linux
---
从去年注册<a href="https://www.nitrous.io/?utm_source=nitrous.io&utm_medium=copypaste&utm_campaign=referral" target=_blank>nitrous</a>到现在，一直都是通过web或者nitrous for chrome来登录，都没有问题。最近在wifi环境下，通过本地linux上的ssh登录，一直超时。同样，在wifi环境下，通过手机的远程登录工具同样失败，换成移动数据网络，就可以成功登录，这个问题困扰我两三天。终于：问题今天下午解决了。

刚开始注册nitrous账号时候，如果选择日本的节点，即: apne1.nitrousbox.com ，从国内连接线路貌似很不稳定，从美国的vps ssh这个地址时候，不会有任何问题。

而如果在注册时候选择美国东部或者美国西部节点，即: usw1-2.nitrousbox.com，这个从国内连接就没问题。

所以我就把节点改成美国.

---
免费linux开发环境 ，直接点击链接就可以了 <a href="https://www.nitrous.io/" target=_blank>nitrous</a>
---

---
免费vpn账号链接：<a href="http://www.djjsq.com" target=_blank>豆荚加速</a>
---
