---
layout: post
date: Wed Apr 23 21:10:13 CST 2014
title: 推荐在线编译器compileonline
categories: 分享
tags: linux
---
推荐个在线编译器： <a href="http://www.compileonline.com/" target=_blank>compileonline</a> ， 对于初学者来说，安装好软件，并运行第一个程序，有时真的是一个很大的负担。在线编译器 compileonline 的使用，能够大大减轻这方面的负担。

如下php示例代码：

<script src="https://gist.github.com/hellokitty111/0ade850c13d342b0c74f.js"></script>

如果想验证代码执行效果，而身边又没有编程、编译环境，就可以通过compileonline来验证了.其中php执行页面：<a href="http://www.compileonline.com/execute_php_online.php" target=_blank>php</a>

![](http://pictureimage.qiniudn.com/code.gif)

PS:这个网站可能有个bug，就是在<a href="http://www.compileonline.com/execute_bash_online.php" target=_blank>bash shell</a> 执行如下代码：

<script src="https://gist.github.com/hellokitty111/11217335.js"></script>

就会删除其中第三行那个用户的工作目录，不要做坏事噢，或者还可以看他们在搞什么东东...

---
免费linux开发环境 ，直接点击链接就可以了 <a href="https://www.nitrous.io/" target=_blank>nitrous</a>
---

                                                                                                                                                               
---                                                                                                                                                            
免费vpn账号链接：<a href="http://www.djjsq.com" target=_blank>豆荚加速</a>                 
---  
