---
layout: post
date: Tue Apr 29 19:22:08 HKT 2014
title: chromebook进入开发者模式并安装ubuntu
categories: 分享
tags: chromebook
---
之前，我的chromebook一直保持原生态，今晚想试试进入开发者模式，然后装上ubuntu(没有装桌面)，现在先把操作步骤记录下来：

- 进入开发者模式(不同型号的chromebook进入方式不同，有的是通过硬件开关，有的是通过快捷键打开)
- 进入命令行模式(ctrl-alt-t打开标签页)
- 进入shell(crosh>shell)
- 管理员(sudo su -)
- 执行dev_install命令安装谷歌开发者工具包

在chromebook开发者模式下，安装谷歌开发者工具包以后，就可以直接使用goagent

下面安装ubuntu系统(只安装命令行模式):

这里有个开源项目，可以参考来安装：<a href="https://github.com/dnschneid/crouton" target=_blank>crouton</a>

我这里说下我自己的执行步骤：

- 首先下载<a href="http://goo.gl/fd3zc" target=_blank>crouton执行脚本</a>，存放到chromebook的Downloads文件夹
- 执行 sh -e ~/Downloads/crouton -t cli后会下载deb包
- 安装好后，在chrome os 的shell下执行sudo enter-chroot进入ubuntu
- 就可以使用apt-get了，这时就可以安装基本的工具如：git、screen、tmux等等.

好，最后来张截图吧：

![](http://pictureimage.qiniudn.com/chromebook_ubuntu.png)

---
免费linux开发环境 ，直接点击链接就可以了 <a href="https://www.nitrous.io/" target=_blank>nitrous</a>
---

---
免费vpn账号链接：<a href="http://www.djjsq.com" target=_blank>豆荚加速</a>
---
