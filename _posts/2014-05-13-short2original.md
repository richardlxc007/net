---
layout: post
date: Tue May 13 20:13:37 HKT 2014
title: 还原短链接为原链接
categories: 分享
tags: linux
---
【通过php将短链接还原为原链接】

代码片段:

<script src="https://gist.github.com/hellokitty111/63504daba678b485ff97.js"></script>

命令行执行: 

- $php short2original.php

或者通过命令行来赋值给脚本中的url，即脚本中行2的注释部分，然后执行:

- $php short2original.php shortUrl

截图:

![](http://pictureimage.qiniudn.com/short2originalUrl.png)

---
免费linux开发环境 ，直接点击链接就可以了 <a href="https://www.nitrous.io/" target=_blank>nitrous</a>
---

---
免费vpn账号链接：<a href="http://www.djjsq.com" target=_blank>豆荚加速</a>
---
