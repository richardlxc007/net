---
layout: post
date: Thu Apr 24 21:24:08 CST 2014
title: 从命令行创建git仓库
categories: 分享
tags: linux
---
之前在<a href="https://www.github.com" target=_blank>github</a>创建仓库 都是通过网页端来搞，有时候很不方便，比如我现在在使用移动网络的热点来上网，不想耗费太多流量，就想着通过命令行来解决一些问题。于是乎想到了curl模拟登录，

```
curl -u 'USER' https://api.github.com/user/repos -d '{"name":"REPO"}'
```

其中user即用户名，REPO即想要的仓库名。

如果想添加描述信息，那就在提交数据部分添加description，即 {"name":"REPO","description":"some description string about this repo"}
如果不想将返回信息输出到终端，curl -s -o out.txt 'USER' ...就可以了，即：

```
curl -s -o out.txt 'USER' https://api.github.com/user/repos -d '{"name":"REPO","description":"some string"}'
```

命令行建立好仓库后执行：

```
git remote add origin git@github.com:USER/REPO.git
```

---
免费linux开发环境 ，直接点击链接就可以了 <a href="https://www.nitrous.io/" target=_blank>nitrous</a>
---
                                                                                                                                                               
---                                                                                                                                                            
免费vpn账号链接：<a href="http://www.djjsq.com" target=_blank>豆荚加速</a>                 
---  
