---
layout: post
date: Thu May  1 10:50:56 CST 2014                                                                                          
title: google发布web designer for linux
categories: 分享
tags: linux
---
Google 推出的所见即所得 HTML5/CSS3 Web 工具最近发布了适用于 Linux 平台的版本。

Google Web Designer 提供如下功能：

- 充分考虑到屏幕自适应问题，考虑了在不同设备上显示网页的需求
- 轻松在设计和源代码模式之间切换
- 允许简便的使用关键帧技术创建动画
- 通过 CSS3 实现 3D 效果支持
- 包含针对 DoubleClick Studio 和 AdMob 的支持向导，亦可载入其他广告支持

<a href="https://www.google.com/webdesigner/index.html" target=_blank>详细介绍及主页</a>

---
免费linux开发环境 ，直接点击链接就可以了 <a href="https://www.nitrous.io/" target=_blank>nitrous</a>
---

---                                                                                                                                                            
免费vpn账号链接：<a href="http://www.djjsq.com" target=_blank>豆荚加速</a>                 
---  
